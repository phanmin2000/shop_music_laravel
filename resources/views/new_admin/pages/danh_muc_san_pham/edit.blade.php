@extends('new_admin.master')
@section('title')
    <h3>Quản Lý Danh Mục</h3>
@endsection
@section('content')
<div id="app">
    <div class="row">
        <div class="col-md-4">
            <div class="card" style="height: auto">
                <div class="card-header">
                    <h4 class="card-title" id="basic-layout-colored-form-control">Cập Nhật danh mục</h4>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <form class="form" action="/admin/danh-muc/update" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{ $danh_muc->id }}">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="userinput1">Tên Danh Mục</label>
                                            <input type="text" class="form-control" value="{{ $danh_muc->ten_danh_muc }}"  name="ten_danh_muc">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="userinput3">Hình Ảnh</label>
                                            <input type="file" class="form-control" value="{{ $danh_muc->hinh_anh }}"  name="hinh_anh"  >
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-12">
                                        <label for="issueinput6">Danh Mục Cha</label>
                                        <select name="id_danh_muc_cha" class="form-control" >
                                            <option value="">Root</option>
                                            @foreach ($danh_muc_cha as $value)
                                                <option value="{{ $value->id }}" {{$value->id = $danh_muc->id_danh_muc_cha ? 'selected' : '' }}>{{ $value->ten_danh_muc }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-12">
                                        <label for="issueinput6">Tình Trạng</label>
                                        <select name="is_open" class="form-control" >
                                            <option value="1" {{ $danh_muc->is_open = 1 ? 'selected' : '' }}>Hiển Thị</option>
                                            <option value="0" {{ $danh_muc->is_open = 0 ? 'selected' : '' }}>Tạm Tắt</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-actions right">
                                <button type="submit" class="btn btn-primary">Cập Nhập Danh Mục
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('js')
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
    $('.lfm').filemanager('image');
</script>
@endsection
