<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDanhMucSanPhamRequest;
use App\Http\Requests\UpdateDanhMuc;
use App\Http\Requests\UpdateDanhMucSanPhamRequest;
use App\Models\DanhMucSanPham;
use App\Models\SanPham;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DanhMucSanPhamController extends Controller
{
    public function index()
    {
        $danhMuc = DanhMucSanPham::where('id_danh_muc_cha' , 0)->get();
        $data = DanhMucSanPham::leftjoin('danh_muc_san_phams as b' , 'danh_muc_san_phams.id_danh_muc_cha' , 'b.id' )
                                ->select('danh_muc_san_phams.*' , 'b.ten_danh_muc as danh_muc_cha')
                                ->get();
        return view('new_admin.pages.danh_muc_san_pham.index' , compact('danhMuc', 'data'));
    }

    public function store(Request $request)
    {
        $slug = Str::slug($request->ten_danh_muc);
        $img = $request->file('hinh_anh');
        $newName = Str::slug($request->ten_san_pham) . '-' . Str::uuid() . '.' . $img->getClientOriginalExtension();
        $img->move('danh_muc',$newName);
        DanhMucSanPham::create([
            'ten_danh_muc'      =>  $request->ten_danh_muc,
            'slug_danh_muc'     =>  $slug,
            'hinh_anh'          =>  $newName,
            'id_danh_muc_cha'   =>  empty($request->id_danh_muc_cha) ? 0 : $request->id_danh_muc_cha,
            'is_open'           =>  $request->is_open,
        ]);

        toastr()->success('Đã Thêm Mới Danh Mục Thành Công!!');

        return redirect()->back();
    }

    public function delete($id)
    {
        $danh_muc = DanhMucSanPham::find($id);
        if($danh_muc) {
            $danh_muc->delete();
            toastr()->success("Đã xóa thành công");
            return redirect('/admin/danh-muc/index');
        } else {
            toastr()->error("Danh mục không tồn tại");
        }
    }

    public function edit($id)
    {
        $danh_muc = DanhMucSanPham::find($id);
        if($danh_muc) {
            $danh_muc_cha = DanhMucSanPham::where('id_danh_muc_cha', 0)->get();
            return view('new_admin.pages.danh_muc_san_pham.edit', compact('danh_muc','danh_muc_cha'));
        } else {
            toastr()->error("Danh mục không tồn tại!");
            return redirect()->back();
        }
    }

    public function update(UpdateDanhMuc $request){
        $data = $request->all();
        $danh_muc = DanhMucSanPham::find($request->id);
        if($danh_muc){
            if($data['id_danh_muc_cha'] == null){
                $data['id_danh_muc_cha'] = 0;
            }
            $danh_muc->update($data);
            toastr()->success("Đã Cập nhật thành công!!");
            return redirect('/admin/danh-muc/index');
        }else{
            toastr()->error("Đã có lỗi hệ thống");
        }
    }

}
