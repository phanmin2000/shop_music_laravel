<?php

namespace App\Http\Controllers;

use App\Http\Requests\KiemTraDuLieuTaoSanPham;
use App\Http\Requests\UpdateSanPhamRequest;
use App\Models\SanPham;
use App\Models\DanhMucSanPham;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SanPhamController extends Controller
{
    public function index()
    {
        $list_danh_muc = DanhMucSanPham::where('is_open', 1)
                                        ->where('id_danh_muc_cha', '<>', 0)
                                        ->get();


        return view('new_admin.pages.san_pham.index', compact('list_danh_muc'));
    }

    public function list()
    {
        $data = SanPham::join('danh_muc_san_phams', 'san_phams.id_danh_muc', 'danh_muc_san_phams.id')
                        ->select('san_phams.*', 'danh_muc_san_phams.ten_danh_muc')
                        ->get();
        return view('new_admin.pages.san_pham.list', compact('data'));
    }

    public function store(KiemTraDuLieuTaoSanPham $bienNhanDuLieu)
    {
        $slug = Str::slug($bienNhanDuLieu->ten_san_pham);
        $img = $bienNhanDuLieu->file('anh_dai_dien');
        $newName = Str::slug($bienNhanDuLieu->ten_san_pham) . '-' . Str::uuid() . '.' . $img->getClientOriginalExtension();
        $img->move('san_pham',$newName);
        $data = $bienNhanDuLieu->all();
        $data['anh_dai_dien']   = $newName;
        $data['slug_san_pham']  = $slug;
        SanPham::create($data);

        toastr()->success('Đã thêm mới thành công');
        return redirect()->back();
    }

    public function delete($id)
    {
        $san_pham = SanPham::find($id);
        if($san_pham) {
            $san_pham->delete();
            toastr()->success('Đã xóa thành công!!');
            return redirect()->back();
        }else{
            toastr()->error("Đã có lỗi hệ thống");
        }
    }

    public function edit($id)
    {

        $list_danh_muc = DanhMucSanPham::where('is_open', 1)
                        ->where('id_danh_muc_cha', '<>', 0)
                        ->get();
        $san_pham = SanPham::find($id);;

        return view('new_admin.pages.san_pham.edit' , compact('san_pham', 'list_danh_muc'));
    }

    public function update(UpdateSanPhamRequest $request)
    {
        $data     = $request->all();
        $san_pham = SanPham::find($data['id']);
        $san_pham->update($data);

        toastr()->success('Đã cập nhật thành công');

        return redirect('/admin/san-pham/list');
    }

    public function loadData()
    {
        $data = SanPham::all();

        return response()->json([
            'danhSachSanPham' => $data,
        ]);
    }
}
